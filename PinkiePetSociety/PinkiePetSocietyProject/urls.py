"""PinkiePetSocietyProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from PinkiePetSocietyApp import views

urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),

    path('user/create:<int:pk>/', views.UserCreate.as_view()),
    path('admin/create:<int:pk>/', views.AdminCreate.as_view()),
    path('pet/create:<int:pk>/', views.PetCreate.as_view()),
    path('user/detail:<int:pk>/', views.UserDetail.as_view()),
    path('admin/detail:<int:pk>:all/', views.AdminAllDetail.as_view()),
    path('pet/detail:<int:pk>:all/', views.PetAllDetail.as_view()),

    path('order/create:<int:pk>/', views.OrderCreate.as_view()),
    path('order/detail:<int:pk>:<int:id>:all/', views.OrderAllDetail.as_view()),

    path('procedures/create:<int:pk>/', views.ProceduresCreate.as_view()),
    path('procedures/delete:<int:pk>:<int:id>/', views.ProceduresDelete.as_view()), 
    path('procedures/detail:<int:pk>:all/', views.ProceduresAllDetail.as_view()),
    path('procedures/update:<int:pk>:<int:id>/', views.ProceduresUpdate.as_view()),

    path('product/create:<int:pk>/', views.ProductCreate.as_view()),
    path('product/delete:<int:pk>:<int:id>/', views.ProductDelete.as_view()), 
    path('product/detail:<int:pk>:all/', views.ProductAllDetail.as_view()),
    path('product/update:<int:pk>:<int:id>/', views.ProductUpdate.as_view())

]
