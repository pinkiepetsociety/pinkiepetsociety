from django.db import models 
from .admin import Admin
from .pet import Pet
from .procedures import Procedures
from .product import Product
 
class Order(models.Model):     
    id = models.AutoField(primary_key=True)     
    admin = models.ForeignKey(Admin, related_name='Order', on_delete=models.CASCADE) 
    pet = models.ForeignKey(Pet, related_name='Order', on_delete=models.CASCADE) 
    date = models.CharField('date', max_length = 30, default='NULL')
    procedures = models.ForeignKey(Procedures, related_name='Order', on_delete=models.CASCADE, null=True)
    product = models.ForeignKey(Product, related_name='Order', on_delete=models.CASCADE, null=True)
    description = models.CharField('description', default='NULL', max_length=255)     
    amount = models.IntegerField('amount', default=0)