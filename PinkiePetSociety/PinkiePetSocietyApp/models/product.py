from django.db import models 
 
class Product(models.Model):     
    id = models.AutoField(primary_key=True)     
    name = models.CharField('name', default='NULL',max_length = 50, unique=True)
    code = models.BigIntegerField ('code', default=0, unique=True)
    brand = models.CharField ('brand', default='NULL', max_length = 50)
    description = models.CharField('description', default='NULL', max_length=255)     
    stocks = models.IntegerField('stocks', default=0) 
    price = models.IntegerField('price', default=0) 