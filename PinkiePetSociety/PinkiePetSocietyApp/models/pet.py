from django.db import models 
import datetime
 
class Pet(models.Model):     
    id = models.AutoField(primary_key=True)     
    name = models.CharField('name', default='NULL',max_length = 50)
    ownerName = models.CharField('ownerName', default='NULL',max_length = 50)
    ownerCellPhone = models.BigIntegerField('ownerCellPhone', default=0) 
    ownerAddress = models.CharField('ownerAddress', max_length = 30, default='NULL')
    petBirthDay = models.CharField('petBirthDay', max_length = 30, default='NULL')
    dateOfEntry = models.CharField('dateOfEntry', max_length = 30, default='NULL')
    race = models.CharField('race', default='NULL',max_length = 50)
    weight = models.DecimalField('weight', default=0, max_digits=5, decimal_places=2) 
    species = models.CharField('species', default='NULL',max_length = 50)
    sex = models.CharField('sex', default='NULL',max_length = 50)