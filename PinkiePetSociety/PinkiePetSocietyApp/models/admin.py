from django.db import models 
import datetime
 
class Admin(models.Model):     
    id = models.AutoField(primary_key=True)     
    name = models.CharField('name', default='NULL',max_length = 50)
    lastName = models.CharField('lastName', default='NULL',max_length = 50)
    birthDay = models.CharField('birthDay', max_length = 30, default='NULL')
    email = models.EmailField('Email', max_length = 100, default='NULL', unique=True)
    cellPhone = models.BigIntegerField('CellPhone', default=0, unique=True) 
    address = models.CharField('Address', max_length = 30, default='NULL')
    inventory = models.BooleanField(default=False)
    history = models.BooleanField(default=False)
    procedures = models.BooleanField(default=False)