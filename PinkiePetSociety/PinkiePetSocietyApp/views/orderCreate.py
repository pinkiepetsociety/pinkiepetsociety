from rest_framework import status, views, generics
from rest_framework.response import Response 
from rest_framework_simplejwt.backends import TokenBackend 
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework.permissions import IsAuthenticated 
from django.conf import settings
 
from PinkiePetSocietyApp.serializers.orderSerializer import OrderSerializer 
 
class OrderCreate(views.APIView): 

    def post(self, request, *args, **kwargs):#debe llegar PK del user, su accen token y JSON con la data del repuesto
        
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id'] != kwargs['pk']:
            stringResponse = {'detail':'No está autorizado para ver esto'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)

        orderSerializer = OrderSerializer(data=request.data)       
        orderSerializer.is_valid(raise_exception=True)
        orderSerializer.save() 

        return Response("Creado exitosamente", status=status.HTTP_201_CREATED)
        #averiguar por que falla
        #return Response(billSerializer.validated_data, status=status.HTTP_201_CREATED)