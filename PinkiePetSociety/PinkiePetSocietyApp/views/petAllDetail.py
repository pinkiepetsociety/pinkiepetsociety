from django.conf import settings 
from rest_framework import generics, status 
from rest_framework.response import Response 
from rest_framework_simplejwt.backends import TokenBackend 
from rest_framework.permissions import IsAuthenticated 
 
from PinkiePetSocietyApp.models.pet import Pet 
from PinkiePetSocietyApp.serializers.petSerializer import PetSerializer 
 
class PetAllDetail(generics.RetrieveAPIView):     
    queryset = Pet.objects.all()     
    serializer_class = PetSerializer     
    permission_classes = (IsAuthenticated,)   

    def get(self, request, *args, **kwargs):
        #debe llegar PK del pet y su accen token
        #RETURN JSON con la data de todos los pets

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id'] != kwargs['pk']:
            stringResponse = {'detail':'No está autorizado para ver esto'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        
        petSerializer = PetSerializer()
        query_result = petSerializer.get_element()

        result = []
        for pet in query_result:
            result.append(petSerializer.to_representation(pet))

        return Response(result, status=status.HTTP_200_OK) 