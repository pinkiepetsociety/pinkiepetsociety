from django.conf import settings
from rest_framework import status, views 
from rest_framework.response import Response 
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer 
 
from PinkiePetSocietyApp.serializers.userSerializer import UserSerializer 
 
class UserCreate(views.APIView): 
    #debe llegar JSON con la data del user a registrar

    def post(self, request, *args, **kwargs): 

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id'] != kwargs['pk']:
            stringResponse = {'detail':'No está autorizado para ver esto'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED) 

        serializer = UserSerializer(data=request.data)    
        serializer.is_valid(raise_exception=True)  
        serializer.save() 

        tokenData = {"userName":request.data["userName"],                       
                     "password":request.data["password"]}         
        tokenSerializer = TokenObtainPairSerializer(data=tokenData)         
        tokenSerializer.is_valid(raise_exception=True)                          
        
        return Response(tokenSerializer.validated_data, status=status.HTTP_201_CREATED)
        #return Response("Creado "+request.data["username"]+" "+request.data["password"]+" "+request.data["name"]+" "+request.data["age"]+" "+request.data["email"])