from django.conf import settings 
from rest_framework import generics, status, views 
from rest_framework.response import Response 
from rest_framework_simplejwt.backends import TokenBackend 
from rest_framework.permissions import IsAuthenticated 
 
from PinkiePetSocietyApp.models.product import Product 
from PinkiePetSocietyApp.serializers.productSerializer import ProductSerializer 
 
class ProductUpdate(views.APIView):     
    queryset = Product.objects.all()     
    serializer_class = ProductSerializer     
    permission_classes = (IsAuthenticated,)   

    def put(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id'] != kwargs['pk']:
            stringResponse = {'detail':'No está autorizado para ver esto'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)                

        product = Product( id = kwargs['id'])

        productSerializer = ProductSerializer(product, data=request.data)
        productSerializer.is_valid(raise_exception=True)        
        updatedProduct= productSerializer.save()        

        return Response(productSerializer.to_representation(updatedProduct), status=status.HTTP_200_OK)