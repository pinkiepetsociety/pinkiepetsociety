from rest_framework import status, views, generics
from rest_framework.response import Response 
from rest_framework_simplejwt.backends import TokenBackend 
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework.permissions import IsAuthenticated 
from django.conf import settings
 
from PinkiePetSocietyApp.models.order import Order 
from PinkiePetSocietyApp.serializers.orderSerializer import OrderSerializer 
 
class OrderAllDetail(generics.RetrieveAPIView):     
    queryset = Order.objects.all()     
    serializer_class = OrderSerializer     
    permission_classes = (IsAuthenticated,)   

    def get(self, request, *args, **kwargs): 
        #debe llegar PK del user y su accen token
        #RETURN JSON con la data de del users que consulta
    
        token = request.META.get('HTTP_AUTHORIZATION')[7:]         
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])         
        valid_data = tokenBackend.decode(token,verify=False) 
 
        if valid_data['user_id'] != kwargs['pk']:             
            stringResponse = {'detail':'Unauthorized Request'}             
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)    

        orderSerializer = OrderSerializer()
        query_result = orderSerializer.get_element(pet_id=kwargs['id'])

        result = []
        for order in query_result:
            result.append(orderSerializer.to_representation(order))

        return Response(result, status=status.HTTP_200_OK)