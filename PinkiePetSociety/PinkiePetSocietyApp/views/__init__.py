from .orderAllDetail import OrderAllDetail
from .orderCreate import OrderCreate

from .proceduresAllDetail import ProceduresAllDetail
from .proceduresCreate import ProceduresCreate
from .proceduresDelete import ProceduresDelete
from .proceduresUpdate import ProceduresUpdate

from .productAllDetail import ProductAllDetail
from .productCreate import ProductCreate
from .productDelete import ProductDelete
from .productUpdate import ProductUpdate

from .userCreate import UserCreate
from .adminCreate import AdminCreate
from .petCreate import PetCreate
from .userDetail import UserDetail
from .adminAllDetail import AdminAllDetail
from .petAllDetail import PetAllDetail