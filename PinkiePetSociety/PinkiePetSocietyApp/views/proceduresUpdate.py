from django.conf import settings 
from rest_framework import generics, status, views 
from rest_framework.response import Response 
from rest_framework_simplejwt.backends import TokenBackend 
from rest_framework.permissions import IsAuthenticated 
 
from PinkiePetSocietyApp.models.procedures import Procedures 
from PinkiePetSocietyApp.serializers.proceduresSerializer import ProceduresSerializer 
 
class ProceduresUpdate(views.APIView):     
    queryset = Procedures.objects.all()     
    serializer_class = ProceduresSerializer     
    permission_classes = (IsAuthenticated,)   

    def put(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id'] != kwargs['pk']:
            stringResponse = {'detail':'No está autorizado para ver esto'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)                

        procedures = Procedures( id = kwargs['id'])

        proceduresSerializer = ProceduresSerializer(procedures, data=request.data)
        proceduresSerializer.is_valid(raise_exception=True)        
        updatedProcedures= proceduresSerializer.save()        

        return Response(proceduresSerializer.to_representation(updatedProcedures), status=status.HTTP_200_OK)