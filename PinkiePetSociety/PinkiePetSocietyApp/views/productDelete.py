from rest_framework import status, views, generics
from rest_framework.response import Response 
from rest_framework_simplejwt.backends import TokenBackend 
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework.permissions import IsAuthenticated 
from django.conf import settings

from PinkiePetSocietyApp.models.product import Product 
from PinkiePetSocietyApp.serializers.productSerializer import ProductSerializer 
 
class ProductDelete(views.APIView):     
    queryset = Product.objects.all()     
    serializer_class = ProductSerializer     
    permission_classes = (IsAuthenticated,)   

    def delete(self, request, *args, **kwargs):
        #debe llegar PK del user y su accen token
        #mensaje de que se elimino el repuesto solicitado

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id'] != kwargs['pk']:
            stringResponse = {'detail':'No está autorizado para ver esto'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        
        productSerializer = ProductSerializer() 

        stringResponse = {'detail':'se elimino correctamente'}
        return Response(productSerializer.delete_element(id=kwargs["id"]), status=status.HTTP_200_OK) 