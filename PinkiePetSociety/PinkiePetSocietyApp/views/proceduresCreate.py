from rest_framework import status, views, generics
from rest_framework.response import Response 
from rest_framework_simplejwt.backends import TokenBackend 
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework.permissions import IsAuthenticated 
from django.conf import settings
 
from PinkiePetSocietyApp.serializers.proceduresSerializer import ProceduresSerializer 
 
class ProceduresCreate(views.APIView): 

    def post(self, request, *args, **kwargs):#debe llegar PK del user, su accen token y JSON con la data

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id'] != kwargs['pk']:
            stringResponse = {'detail':'No está autorizado para ver esto'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED) 

        proceduresSerializer = ProceduresSerializer(data=request.data)         
        proceduresSerializer.is_valid(raise_exception=True)         
        proceduresSerializer.save() 
        
        return Response(proceduresSerializer.validated_data, status=status.HTTP_201_CREATED)
        #return Response("Creado perro"+request.data["username"]+" "+request.data["password"]+" "+request.data["name"]+" "+request.data["age"]+" "+request.data["email"])