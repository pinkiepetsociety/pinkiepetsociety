from django.contrib import admin
from .models.admin import Admin
from .models.pet import Pet
from .models.product import Product
from .models.user import User

admin.site.register(Admin) 
admin.site.register(Pet)
admin.site.register(Product) 
admin.site.register(User)

# Register your models here.
