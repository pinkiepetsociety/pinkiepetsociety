from PinkiePetSocietyApp.models.pet import  Pet


from rest_framework import serializers 
 
class PetSerializer(serializers.ModelSerializer):     
    class Meta:         
        model =  Pet         
        fields = ['id', 'name', 'ownerName', 'ownerCellPhone', 'ownerAddress', 'petBirthDay', 'dateOfEntry', 'race', 'weight', 'species', 'sex']

    def create(self, validated_data):
        petInstance = Pet.objects.create(**validated_data) 
        return petInstance

    def to_representation(self, obj):         
        pet = Pet.objects.get(id=obj.id)                   
        return {          
            'id' : pet.id,  
            'name' : pet.name,
            'ownerName': pet.ownerName  
        } 
    
    def get_element(self, **obj):        
        return Pet.objects.filter(**obj)