from PinkiePetSocietyApp.models.product import  Product 
from rest_framework import serializers 
 
class ProductSerializer(serializers.ModelSerializer):     
    class Meta:         
        model =  Product         
        fields = ['id', 'name', 'code', 'brand', 'description', 'stocks', 'price'] 

    def create(self, validated_data):              
        productInstance = Product.objects.create(**validated_data)        
        return productInstance 
 
    def to_representation(self, obj):         
        product = Product.objects.get(id=obj.id)                      
        return {                     
            'id': product.id,                      
            'name': product.name,                     
            'code': product.code,  
            'brand' : product.brand,   
            'description' : product.description,
            'stocks': product.stocks,         
            'price' : product.price                         
        }

    def get_element(self, **obj):        
        return Product.objects.filter(**obj)

    def delete_element(self, id):
        product = Product.objects.get(id=id)
        product.delete()
        return {                     
            'id': product.id,                      
            'name': product.name,                     
            'code': product.code,  
            'brand' : product.brand,   
            'description' : product.description,
            'stocks': product.stocks,         
            'price' : product.price                         
        }