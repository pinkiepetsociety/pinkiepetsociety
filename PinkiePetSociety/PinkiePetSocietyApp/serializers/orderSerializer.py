from PinkiePetSocietyApp.models.order import  Order 
from PinkiePetSocietyApp.models.admin import  Admin
from PinkiePetSocietyApp.serializers.adminSerializer import AdminSerializer
from PinkiePetSocietyApp.models.pet import  Pet
from PinkiePetSocietyApp.serializers.petSerializer import PetSerializer
from PinkiePetSocietyApp.models.product import  Product
from PinkiePetSocietyApp.serializers.productSerializer import ProductSerializer
from PinkiePetSocietyApp.models.procedures import  Procedures
from PinkiePetSocietyApp.serializers.proceduresSerializer import ProceduresSerializer
from rest_framework import serializers 
 
class OrderSerializer(serializers.ModelSerializer):     
    class Meta:         
        model =  Order         
        fields = ['id', 'description', 'date', 'admin', 'pet', 'product', 'procedures', 'amount'] 

    def create(self, validated_data):              
        orderInstance = Order.objects.create(**validated_data)        
        return orderInstance 
 
    def to_representation(self, obj):         
        order = Order.objects.get(id=obj.id)   
        admin = AdminSerializer()
        admin = order.admin
        pet = PetSerializer()
        pet = order.pet
        product = ProductSerializer()
        product = order.product
        productJSON="NA"
        procedures = ProceduresSerializer()
        procedures = order.procedures
        proceduresJSON="NA"
        orderJSON ={
            'id': order.id,                      
            'admin': admin.name,                     
            'pet': pet.name,  
            'date' : order.date,
        }
        price = 0
        if product : #si conteien algo retorna TRUE, si es nulo retorna FALSE
            productJSON=product.name
            price = product.price
        else :
            proceduresJSON=procedures.name
            price = procedures.price
        orderJSON['product'] = productJSON
        orderJSON['procedures'] = proceduresJSON
        orderJSON['amount'] = order.amount
        orderJSON['description'] = order.description
        orderJSON['price'] = price
        return orderJSON

    def get_element(self, **obj):     
        return Order.objects.filter(**obj)

    def delete_element(self, id):
        order = Order.objects.get(id=obj.id)   
        order.delete()
        admin = AdminSerializer()
        admin = order.admin
        pet = PetSerializer()
        pet = order.pet
        product = ProductSerializer()
        product = order.product
        productJSON={None}
        procedures = ProceduresSerializer()
        procedures = order.procedures
        proceduresJSON={None} 
        orderJSON ={
            'id': order.id,                      
            'admin': admin.name,                     
            'pet': pet.name, 
            'date' : order.date,
        }
        price = 0
        if product != None:
            productJSON=product.name
            price = product.price
        else :
            proceduresJSON=procedures.name
            price = Procedures.price
        orderJSON['product'] = productJSON
        orderJSON['procedures'] = proceduresJSON
        orderJSON['amount'] = order.amount
        orderJSON['description'] = order.description
        orderJSON['total'] = price
        return orderJSON