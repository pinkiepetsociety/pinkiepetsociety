from PinkiePetSocietyApp.models.procedures import  Procedures 
from rest_framework import serializers 
 
class ProceduresSerializer(serializers.ModelSerializer):     
    class Meta:         
        model =  Procedures         
        fields = ['id', 'name', 'code', 'description', 'price'] 

    def create(self, validated_data):              
        proceduresInstance = Procedures.objects.create(**validated_data)        
        return proceduresInstance 
 
    def to_representation(self, obj):         
        procedures = Procedures.objects.get(id=obj.id)                      
        return {                     
            'id': procedures.id,                      
            'name': procedures.name,                     
            'code': procedures.code,  
            'description' : procedures.description, 
            'price' : procedures.price                         
        }

    def get_element(self, **obj):        
        return Procedures.objects.filter(**obj)

    def delete_element(self, id):
        procedures = Procedures.objects.get(id=id)
        procedures.delete()
        return {                     
            'id': procedures.id,                      
            'name': procedures.name,                     
            'code': procedures.code,  
            'description' : procedures.description, 
            'price' : procedures.price                         
        }