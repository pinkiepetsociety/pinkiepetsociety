from PinkiePetSocietyApp.models.user import  User
from PinkiePetSocietyApp.models.admin import  Admin
from PinkiePetSocietyApp.serializers.adminSerializer import AdminSerializer
from PinkiePetSocietyApp.models.pet import  Pet
from PinkiePetSocietyApp.serializers.petSerializer import PetSerializer

from rest_framework import serializers 
 
class UserSerializer(serializers.ModelSerializer):     
    class Meta:         
        model =  User         
        fields = ['id', 'userName', 'password', 'admin', 'pet']

    def create(self, validated_data):
        userInstance = User.objects.create(**validated_data) 
        return userInstance

    def to_representation(self, obj):         
        user = User.objects.get(id=obj.id)
        userJSON={
            'id' : user.id,          
            'userName': user.userName
        }  
        admin = user.admin   
        adminJSON={ None }
        if admin != None:
            adminJSON ={
                'name' : admin.name,
                'lastName' : admin.lastName,
                'inventory' : admin.inventory,
                'history' : admin.history,
                'procedures' : admin.procedures
            }  
        pet = user.pet   
        petJSON ={ None }
        if pet != None:
            petJSON ={
                'id' : pet.id,
                'name' : pet.name,
                'ownerName' : pet.ownerName,
                'ownerCellPhone' : pet.ownerCellPhone,
                'ownerAddress' : pet.ownerAddress,
                'petBirthDay' : pet.petBirthDay,
                'dateOfEntry' : pet.dateOfEntry,
                'race' : pet.race,
                'weight' : pet.weight,
                'species' : pet.species,
                'sex' : pet.sex
            }  
        userJSON['admin'] = adminJSON
        userJSON['pet'] = petJSON
        return userJSON