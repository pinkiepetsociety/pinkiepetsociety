from PinkiePetSocietyApp.models.admin import  Admin

from rest_framework import serializers 
 
class AdminSerializer(serializers.ModelSerializer):     
    class Meta:         
        model =  Admin         
        fields = ['id', 'name', 'lastName', 'birthDay', 'email', 'cellPhone', 'address', 'inventory', 'history', 'procedures']

    def create(self, validated_data):
        adminInstance = Admin.objects.create(**validated_data) 
        return adminInstance

    def to_representation(self, obj):         
        admin = Admin.objects.get(id=obj.id)                   
        return {          
            'id' : admin.id,
            'name' : admin.name,
            'lastName' : admin.lastName     
        } 

    def get_element(self, **obj):        
        return Admin.objects.filter(**obj)