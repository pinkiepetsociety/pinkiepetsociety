import { createRouter, createWebHistory } from 'vue-router' 
import App from './App.vue' 
 
import Main from './components/Main.vue'
import Inventory from './components/Inventory.vue'
import History from './components/History.vue'
import Procedures from './components/Procedures.vue'
import PetData from './components/PetData.vue'

const routes = [
  {         
    path: '/',         
    name: 'root',         
    component: App     
  },
  {         
    path: '/PinkiePetSociety/',         
    name: "Main",         
    component: Main     
  },
  {         
    path: '/PinkiePetSociety/Inventory/',         
    name: "Inventory",         
    component: Inventory     
  },
  {         
    path: '/PinkiePetSociety/History/',         
    name: "History",         
    component: History     
  },
  {         
    path: '/PinkiePetSociety/Procedures/',         
    name: "Procedures",         
    component: Procedures     
  },
  {         
    path: '/PinkiePetSociety/PetData/',         
    name: "PetData",         
    component: PetData     
  }
  
] 
 
const router = createRouter({     
  history: createWebHistory(),     
  routes 
}) 
 
export default router